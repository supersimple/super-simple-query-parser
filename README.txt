//*****SCRIPT DETAILS***********************************************************************/

Super Simple Query Parser
Released: Nov 2006
Author(s): Todd Resudek - tr1design.net

Notes:
This code is open-source, free to use privately or commercially.

This script will parse any query-string variables that are set in the URL and
make them accessible via a javascript associative array.

The purpose is to allow manipulation of a page without using server-side technologies.

Code is for modern browsers and tested on:
safari, firefox, mozilla, netscape 7, gecko, opera 7+, mac ie5.2, win ie6

This script was written for supersimple.org


//*****INSTALLING***************************************************************************/

Just include the .js file in any page that you want to be able to access the query-string
params. After that, you will be able to use those values in other javascripts in the page.






