/******************************************************************************

parseParams() function written by Todd Resudek for Supersimple.org
Released November 2006

This function will parse any query string parameters into an array of key-value
pairs. 

The purpose is to be able manipulate a page, using variables, without the need
for a server-side technology.

The script runs itself onload, and stores all params into an associative array.
To access a specific value, use params[key]; (eg. alert(params['product']); )


******************************************************************************/

//calls the parse function when the window loads
//remove this line if you want to call the parsing manually
window.onload = function(){parseParams();}

function parseParams(){
	
	//store the current window location
	var loc = window.location.toString();
		
	//grab the query string from the location	
	var queriesB = loc.indexOf('?');
	var queries = loc.substring(queriesB +1, loc.length);
	
	//break up into array of pairs
	var query = queries.split('&');
	
	//init an array to hold the key/value pairs
	params = new Array();
	
	for(var i=0; i < query.length; i++){
		//split into key value pairs
		curVar = query[i].split('=');
		
		//make a new hash of those key/value pairs
		params[curVar[0]] = curVar[1];
	}

}